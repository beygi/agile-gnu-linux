echo "configuring persian distro"
echo "install subtitle fixer by aliva"
wget https://github.com/aliva/SubtitleFixer/raw/master/install.sh \
-O subtitlefixer-install.sh \
&& chmod +x+u subtitlefixer-install.sh \
&& ./subtitlefixer-install.sh
rm -r /tmp/SubtitleFixer
rm /subtitlefixer-install.sh
rm /usr/share/gnome-shell/open-search-providers/google.xml
echo "install saghar and negar"

cd /debs


if [ "$ARCH" == "i386" ]
then
	sudo dpkg -i Saaghar-1.0.94-1Build1975_i386.deb
fi

if [ "$ARCH" == "amd64" ]
then
	sudo dpkg -i Saaghar-1.0.94Build1975_amd64.deb
fi

sudo apt-get -f install
sudo dpkg -i saaghar-data-59.90.07.deb
sudo apt-get -f install
sudo dpkg -i gnegar_0.1.1_all.deb
sudo apt-get -f install
cd /
