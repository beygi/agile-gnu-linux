const WorkspaceSwitcherPopup = imports.ui.workspaceSwitcherPopup;

const Meta = imports.gi.Meta;
const St = imports.gi.St;
let redraw, redisplay;

function init() {
}

function enable() {
    redraw = WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redraw;
    redisplay = WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redisplay;

    // GNOME 3.2, 3.4
    WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redraw = function (direction, activeWorkspaceIndex) {
        redraw.apply(this, arguments);
        let children = this._list.get_children();
        for (let i = 0; i < children.length; ++i) {
            if (i !== activeWorkspaceIndex) {
                children[i].child = new St.Label({
                    text: Meta.prefs_get_workspace_name(i),
                    style_class: 'ws-switcher-label'
                });
            }
        }
    };

    // GNOME 3.6
    WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redisplay = function () {
        redisplay.apply(this, arguments);
        let children = this._list.get_children();
        for (let i = 0; i < children.length; ++i) {
            if (i !== this._activeWorkspaceIndex) {
                children[i].child = new St.Label({
                    text: Meta.prefs_get_workspace_name(i),
                    style_class: 'ws-switcher-label'
                });
            }
        }
    };
}

function disable() {
    WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redraw = redraw;
    WorkspaceSwitcherPopup.WorkspaceSwitcherPopup.prototype._redisplay = redisplay;
}
