
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const St = imports.gi.St;
const GLib = imports.gi.GLib;
const Shell = imports.gi.Shell;

const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;
const ExtensionsPath = GLib.build_filenamev([global.userdatadir, 'extensions/']);

const Main = imports.ui.main;
const Panel = imports.ui.panel;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;

const ESMenuItem = new Lang.Class({
    Name: 'ESMenu.ESMenuItem',
    Extends: PopupMenu.PopupBaseMenuItem,

    _init: function(esmenu) {
	this.parent();

	this.label = new St.Label({ text: esmenu.get_name() });
	this.addActor(this.label);
        this.actor.label_actor = this.label;

	this.esmenu = esmenu;

	let ESIcon = new St.Icon({ icon_name: 'preferences-other-symbolic',
				      style_class: 'popup-menu-icon ' });
	let ESButton = new St.Button({ child: ESIcon });
	this.addActor(ESButton);
    }

    
});

const ESMenu = new Lang.Class({
    Name: 'ESMenu.ESMenu',
    Extends: PanelMenu.SystemStatusButton,

    _init: function() {
	this.parent('preferences-other-symbolic', _("ESMenuItem"));

	this._monitor = Gio.VolumeMonitor.get();

	this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
	this.menu.addAction(_("Installed Extensions"), function(event) {
	    Main.Util.trySpawnCommandLine('xdg-open https://extensions.gnome.org/local/');
	});

	this.menu.addAction(_("Browse Extensions"), function(event) {
	    Main.Util.trySpawnCommandLine('xdg-open https://extensions.gnome.org/');
	});

	this.menu.addAction(_("Extension Preferences"), function(event) {
	    Main.Util.trySpawnCommandLine('gnome-shell-extension-prefs');
	});

	this.menu.addAction(_("Advanced Settings"), function(event) {
	    let appSystem = Shell.AppSystem.get_default();
	    let app = appSystem.lookup_app('gnome-tweak-tool.desktop');
	    app.activate_full(-1, event.get_time());
	});

	this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

	this.menu.addAction(_("Open Extensions Folder"), function(event) {
	    Main.Util.trySpawnCommandLine('xdg-open ' + ExtensionsPath);
	});

	this.menu.addAction(_("Restart Shell"), function(event) {
	    global.reexec_self();
	});

	this.menu.addAction(_("Looking Glass"), function(event) {
	    Main.createLookingGlass().toggle();
	});

	this._updateMenuVisibility();

    },

    _updateMenuVisibility: function() {
        this.actor.show();
    },

    destroy: function() {
        if (this._connectedId) {
	    this._monitor.disconnect(this._connectedId);
	    this._monitor.disconnect(this._disconnectedId);
            this._connectedId = 0;
            this._disconnectedId = 0;
        }

	this.parent();
    },
});

function init() {
    Convenience.initTranslations();
}

let _indicator;

function enable() {
    _indicator = new ESMenu;
    Main.panel.addToStatusArea('ES-menu', _indicator);
}

function disable() {
    _indicator.destroy();
}
