echo 'Configuring gnome-shell ...'
echo 'enable all extensions in ~/.local/share/gnome-shell/extensions'
echo "[org.gnome.shell]" >> /usr/share/glib-2.0/schemas/arios.gschema.override


find  /etc/skel/.local/share/gnome-shell/extensions/  /usr/share/gnome-shell/extensions/ -maxdepth 1 -mindepth 1 -printf "%f\n" -type d | xargs -I{} echo "'{}'" | sed ':a;N;$!ba;s/\n/,/g' | xargs -0 -I{} echo "enabled-extensions=[{}]" | sed ':a;N;$!ba;s/\n//g' >> /usr/share/glib-2.0/schemas/arios.gschema.override
glib-compile-schemas /usr/share/glib-2.0/schemas
#---------------------------------------------------------------------------------------

echo "configuring splash"
sudo update-alternatives --install /lib/plymouth/themes/default.plymouth default.plymouth /lib/plymouth/themes/AriOS/AriOS.plymouth 110
sudo apt-get remove --purge plymouth-theme-ubuntu-logo
rm -r /lib/plymouth/themes/ubuntu-logo
sudo update-alternatives --config default.plymouth
sudo update-alternatives --set default.plymouth /lib/plymouth/themes/AriOS/AriOS.plymouth
sudo update-initramfs -u
echo "install grml config for zsh"
cd /etc/skel
wget -O .zshrc http://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
cd /
echo "changing permissions for /usr/share/arios"
chmod -R 777 /usr/share/arios
echo "remove non-free packages installed by wine"
apt-get -f -y purge ttf-mscorefonts-installer
apt-get -f -y purge unrar
cd /usr/share
chmod -R 777 ubiquity-slideshow
cd /debs


if [ "$ARCH" == "i386" ]
then
	sudo dpkg -i python-gtop_2.32.0_i386.deb
fi

if [ "$ARCH" == "amd64" ]
then
	sudo dpkg -i python-gtop_2.32.0_amd64.deb
fi

sudo apt-get -f install
sudo apt-get install -f -y awn-applets-all
cd /

